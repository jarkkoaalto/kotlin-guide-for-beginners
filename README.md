# Kotlin-guide-for-beginners

One hour Kotlin guide for beginners is designed for students who are familiar with Object Oriented Programming(OOP).

You will learn the basic knowledge of Kotlin language and OOP programming with Kotlin.

Course content

###### Section 01: Introduction
###### Section 02: Kotlin Basic
###### Section 03: Kotlin OOP