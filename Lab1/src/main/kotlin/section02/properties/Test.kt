package section02.properties

/*
    in get and set ,we can use the property value by the field keyword
 */

var name: String = "Hello"
    get() = field
    set(value){field = value}

val no: Int = 10
    get() = field


var age: Int = 0
    set(value){
        if(value<0){
            field=0
        }else{
            field = value
        }
    }

var name1: String? = null
    get() {
        return field?.toUpperCase()
    }

fun main(args: Array<String>){
    println("$name, $no")
    name="Hi there"
    println("$name, $no")
}