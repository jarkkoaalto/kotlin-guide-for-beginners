package section02.two

/* Variable demonstration*/
val data1: Int = 10

class MyClass {
    val data: Int = 10
    var data2: String? = null

    var data3: String = "one"

    fun some() {
        val data: Int
        data = 10
    }
}
