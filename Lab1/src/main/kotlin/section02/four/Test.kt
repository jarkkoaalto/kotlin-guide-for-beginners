package section02.four

fun main(args: Array<String>) {
    var array = arrayOf(1,"one", true)
    array[0] = 10
    array[1] = "World"
    println("${array[0]}...${array[1]} ... ${array[2]}")
    println("size:${array.size}...${array.get(0)}...${array.get(1)}...${array.get(2)}")

    var arrayInt = arrayOf<Int>(10,20,30)

    var arrayInt2 = intArrayOf(10,20,40)

    println(arrayInt[1])
    println(arrayInt2[2])

    var array2 = arrayOfNulls<Any>(3)
    array2[0] = 10
    array2[1] = "hi there"
    array2[2] = true

    println("${array2[0]} .. ${array2[1]}..${array2[2]}")

    var emptyArray=Array<String>(3,{""})
    emptyArray[0] = "hello"
    emptyArray[1] = "world"
    emptyArray[2] = "hi there"
    println("${emptyArray[0]}..${emptyArray[1]}..${emptyArray[2]}")


    /**
     * Collection type
     * List,set and Map type are an interface that inherits Collection
     * Kotling disatinguished mutable and immutable collections.
     * An object of kotlin.collection.list is immutable and porovides only size() and get() functions
     * and doesen't provide functions like add(), set() which changes data.
     *
     * An objects of kotlin.collection mutablelist is mutable and provides size() and get() functions as well as add(),set()
     */

    var list1 = listOf<Int>(10,20)
    list1.forEach {
        println(it)}

    var list2 = mutableListOf<Int>(10,20,30)
    list2.add(50)


}