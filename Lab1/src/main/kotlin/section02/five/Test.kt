package section02.five


/*
    Function overloading
    Multible functions with the same name but different parameters can de declared
 */

fun some(a: Int, b:Int):Int = a+b

fun sayHello(name: String = "Hi there", no: Int){
    println("hello $name")
}

fun main(args: Array<String>){
    some(44, 44)
    sayHello("one", 10)
}

