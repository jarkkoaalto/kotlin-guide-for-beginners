package section02.ten

/*
    Marking a property with the lateinit modifier allows us to initialize the property after
    creating a object

 */

/*
 Lazy if we use lazy keyword, we don't need to initialize the property until it is used.
 */


class User{
    lateinit var data: String

    val data2 : String by lazy {
        println("lazy ....")
        "Hi Lazy"
    }
}

fun main(args: Array<String>){
    val obj = User()
    obj.data = "Hi there"
}

