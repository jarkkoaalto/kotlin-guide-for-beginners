package section02.loops

fun main(args: Array<String>) {
    for(i in 10 downTo  1 step 2){
        println(i)
    }
    val list = listOf<String>("Hi there"," !!")
    for((index, value) in list.withIndex()){
        println(" Index : $index, value : $value")
    }

    for(i in 1..3){
        for(j in 1..3){
            if(j>1) break
            println("i: $i, j:$j")
        }
    }

    var x2 = 0
    var sum2 = 0
    while(true){
        sum2 += ++x2
        if(x2==10)break
    }
    println(sum2)
}