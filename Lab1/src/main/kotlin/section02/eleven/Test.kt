package section02.eleven

/*

    null safely call operator ?
    a nullable type property can hold null so it can cause NPE
    Operator must use also var.
 */

/*
 Null safe cast: save caset by as= operator return null if the cast is not successful
*/
fun main(args: Array<String>){

    var data1:  String? = "one"
    var length: Int? = data1?.length



}

