package section02.three

import java.lang.Exception

/*Data types, type checks and casting */
val data1: String = "hello"

val data2: String = """
            hello
            world
            $data1
            """

    /*
    * Unit type:
    * Unit type can be a return type when there  is no return statement.
    * It's vary close to void in Java. (but no same)
    * */
fun test(): Unit{

}

    /*
    *   Nothing type
    *   Nothing type is used when we want to show explicity there is no value
     */
fun myFun(arg: Nothing?): Nothing{
            throw Exception()
    }

    /*
    * Type Check
    * We can check type by is operator. The operator is checking type and casts automatically when needed - smart casting   *
     */
fun getStringLength(obj: Any): Int? {
        if(obj is String){
            return obj.length
        }
        return null
    }

    /*
    * !is is a negated form of is operator
     */
fun getStringLength2(obj: Any): Int? {
        if(obj !is String) return null
        return obj.length
    }

    /*
    * Type casting: Kotlin doesen't support implicit conversions for numbers
        var a1: Int = 10
        var a2: Double = a1 // Error
    */
fun main(args: Array<String>){
    println(data1)
    println(data2)

    val result = test()
    println(result)




}