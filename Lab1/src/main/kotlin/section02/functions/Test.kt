package section02.functions


/*
    conditional expression
       Expression consist of variables, constants, operators or functions that evaluate to single value.
       Statement consist of characters and symbols that make up a complete unit of execurion.
 */

fun some(a: Int){
    val result = if(a > 10) "hello"; else "wrong"
}

/*
    When operators replace the switch operators of C or Java
 */
val a2 = 1

fun testAny(arg: Any) {
    when (a2) {
        1 -> println("a2 == 1")
        2 -> println("a2 == 2")
        else -> {
            println("a2 in neither 1 nor 2")

        }
    }
/*
    An argument of when operators can be many different types more than just Int type.
 */

    val data1 = "hello"
    when (data1) {
        "hello" -> println("data1 is hello")
        "world" -> println("data1 is world")
        else -> println("data1 is not hello or world")
    }


when(arg){
    1 -> println("arg is 1")
    10, 20 -> println("arg is 10 or 20")
    30 -> {
        val result = arg as Int * 10
        println("arg is 30, result is ${result}")
    }
    "hello" -> println("args is hello")
    is Int  -> println("arg type is Int but is not 1, 10, 20 ,30")
    is String -> println("arg type is String but is hello")
    else -> println("Unknown data")
}
}

fun main(args: Array<String>){

}