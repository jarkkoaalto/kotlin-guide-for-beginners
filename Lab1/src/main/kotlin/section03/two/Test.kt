package section03.two

/*
    Inheritance
    supercalss have to be declared with open keyword
    to declare a supercalss explicity, we place it the right hand side of colon(:)

    Function override
    override meand rewriting superclass functions of properties in subclass

 */

open class Shape{
    var x: Int = 0
    var y: Int = 0

    fun print(){
        println("location :$x, $y")
    }
}

class Rect: Shape() {
    var width = 0
    var height = 0
}