package section03.four

/*
    Visibility modifier: there are four visibility modifiers in Kotlin: public, internal, protected and private.
 */

class Test{
    var data: Int = 10
    fun some():Int{
        return data
    }
    var data2: Int = 10
       private set(value){field = value}
}

fun main(args: Array<String>){
    val obj = Test()
    obj.some()

    println(obj.data2)
}

