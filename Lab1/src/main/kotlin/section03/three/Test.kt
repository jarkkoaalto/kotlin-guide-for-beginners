package section03.three

/*
    Abstract class is a classs that has abstract functions or abstract properites.
    we cant create an object of an abstract class

    Interface is to declare abstract functions

 */

abstract class Super{
    abstract val data2 : Int
    abstract fun myFun()
}

class Sub:Super(){
    override val data2: Int = 10
    override fun myFun() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

interface Myinterface{
    var data1: String
    fun myFun()
}
class Some: Myinterface{
    override var data1: String
    get() = TODO("not implemented")
    set(value){}

    override fun myFun() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

fun main(args: Array<String>){
    val obj = Sub()
}

