package section03.one

import section02.two.MyClass

class MyClass {
    var name: String =" Hi there"
    fun SayWorld(){
        print("World $name")
    }
}
// Class variables if add var or val id: String .....
class User(var id: String,var name: String){
    init{
        println("construcrot: id = $id, name $name")
    }
    fun some(){
        println("Some : id = $id, name = $name")
    }
}

/*
    Secondary constructors are prefixed with constructor keywod in the class body
    this() to delegate to another constructor of the same calss this keyword
 */
class User1(name:String){
    constructor(name: String, age: Int): this(name){

    }

}



fun main(args: Array<String>){
    val obj = User("one", "Jeff")
    obj.some()
    val obj1 = MyClass()



}